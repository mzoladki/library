from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DeleteView, View
from django.views.generic.edit import FormView
from .forms import BookForm, AuthorForm, CategoryForm
from .models.books import Book
from .models.authors import Author
from .models.categories import Category
import json
from django.core import serializers
from django.urls import reverse_lazy


class Main(TemplateView):
    template_name = "main.html"

class AddBook(FormView):
    template_name = 'add_book.html'
    form_class = BookForm
    success_url = '/books/list-books'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

class AddAuthor(FormView):
    template_name = 'add_author.html'
    form_class = AuthorForm

class AddCategory(FormView):
    template_name = 'add_category.html'
    form_class = CategoryForm

class BooksList(ListView):
    template_name = 'book_list.html'
    model = Book

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        context = {}
        context['object_list'] = [
            {
                'author': [author.name for author in i.authors.all()],
                'title': i.title,
                'description': i.description if i.description else "",
                'pk': i.pk,
                'category': [category.name for category in i.category.all()]
            } for i in data['object_list'] ]
        return context

class DeleteBook(DeleteView):
    model = Book
    template_name = "delete_book.html"
    success_url = reverse_lazy('list-books')

class ImportBooks(View):
    template_name = 'import_book.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        for key, value in request.POST.items():
            if key != "csrfmiddlewaretoken":
                model = json.loads(value)

                book, created = Book.objects.get_or_create(title=model['title'], description=model['description'])

                for item in model['author']:
                    print('-----')
                    obj, created = Author.objects.get_or_create(name=item)
                    print(obj)
                    print(type(obj))
                    print('-----')

                    book.authors.add(obj)
                
                for item in model['categories']:
                    obj, created = Category.objects.get_or_create(name=item)
                    book.category.add(obj)

                book.save()
        return render(request, self.template_name)
