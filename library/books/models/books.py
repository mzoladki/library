from django.db import models
from .authors import Author
from .categories import Category

class Book(models.Model):
    authors = models.ManyToManyField(Author)
    category = models.ManyToManyField(Category)
    title = models.CharField("Title", max_length=120)
    description = models.TextField("Description", blank=True, null=True)

    class Meta:
        ordering = ('title',)
        unique_together = ["title", "description"]

    def __str__(self):
        return self.title
