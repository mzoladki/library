from django.db import models

class Author(models.Model):
    name = models.CharField("Name", max_length=120)

    class Meta:
        ordering = ('name',)
        unique_together = ['name']

    def __str__(self):
        return self.name