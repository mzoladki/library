from django import forms
from .models.books import Book
from .models.authors import Author
from .models.categories import Category

class BookForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = ['authors', 'category', 'title', 'description']

class AuthorForm(forms.ModelForm):

    class Meta:
        model = Author
        fields = ['name', ]

class CategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ['name', ]