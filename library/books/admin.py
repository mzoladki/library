from django.contrib import admin
from books.models.authors import Author
from books.models.books import Book
from books.models.categories import Category
 
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Book)
