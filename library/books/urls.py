from django.contrib import admin
from django.urls import path
from .views import Main, AddBook, AddAuthor, AddCategory, BooksList, DeleteBook, ImportBooks

urlpatterns = [
    path('', Main.as_view(), name='main'),
    path('add-book', AddBook.as_view(), name='add-book'),
    path('add-author', AddAuthor.as_view(), name='add-author'),
    path('add-category', AddCategory.as_view(), name='add-category'),
    path('list-books', BooksList.as_view(), name='list-books'),
    path('delete-book/<int:pk>', DeleteBook.as_view(), name='delete-book'),
    path('import-books', ImportBooks.as_view(), name='import-books')
]
